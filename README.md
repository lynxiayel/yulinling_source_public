# Yulinling
# Structure

## Page Naming Conventions
* English: all lowercase separated with underscore: this_is_a_post.md
* Chinese: all lowercase of quanpin, no separator: zheshiyigetiezi.md

## Sections
* Album
* Post
* Search_result (used for showing search result only, do not modify)

## Shortcodes
* mfig

    {{<mfig src="src_path" alt="alt_text" class="class" link="link" caption="caption" attr="attr" attrlink="attrlink">}}

* code

    {{<code "lang">}}
    my code here
    {{/code}}

* music

    * type: mp3, m4a, ogg.  
    {{<music src="src_path" type="audio_type" artist="artist" title="audio_title">}}

* video

    * type: mp4, ogg, webm.
    * height: if not given, will use "auto".  
    {{<video src="src_path" type="video_type" height="height">}}

* timeline

    * name: used as the id of the div placeholder.
    * src: gives the data path, local data should be a json file, say d.json. It's suggested to it put under post/post_name/, then we can give src="d.json". Hugo will move the data file to the right place upon publishing.
    * first: give a random value to denote its the first timeline object in the page and will load corresponding js and css files.  
    {{<timeline name="name" src="data_path" first="whatever" >}}
* initAlbumNanoGallery
    * dir: dir for current album, for example, the album dir chengdu in source code is at content/album/chengdu, then you should give dir="content/album/chengdu".  
    {{<initAlbumNanoGallery dir="album_dir">}}
    
* initAlbumSlider

    * dir: same as initAlbumNanoGallery.  
    {{<initAlbumSlider dir="album_dir">}}

* filelist

    * will list a table showing each file's info under the given dir.  
    {{filelist dir="dir"}}

